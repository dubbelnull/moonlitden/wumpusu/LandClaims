package net.pl3x.bukkit.claims.claim.task;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import net.pl3x.bukkit.claims.WildClaims;
import net.pl3x.bukkit.claims.claim.Claim;
import net.pl3x.bukkit.claims.configuration.Config;
import net.pl3x.bukkit.claims.configuration.Lang;
import net.pl3x.bukkit.claims.player.Pl3xPlayer;

public class DeleteInactiveClaims extends BukkitRunnable {
    private final WildClaims plugin;

    public DeleteInactiveClaims(WildClaims plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        if (Config.DELETE_INACTIVE_CLAIMS <= 0) {
            return;
        }

        Set<Pl3xPlayer> targets = new HashSet<>(plugin.getClaimManager().getTopLevelClaims()).stream()
                .filter(claim -> claim.getLastActive() > Config.DELETE_INACTIVE_CLAIMS)
                .map(claim -> plugin.getPlayerManager().getPlayer(claim.getOwner()))
                .collect(Collectors.toSet());

        targets.forEach(target -> new DeleteClaims(target).runTask(plugin));
    }

    private class DeleteClaims extends BukkitRunnable {
        private final Pl3xPlayer pl3xPlayer;

        private DeleteClaims(Pl3xPlayer pl3xPlayer) {
            this.pl3xPlayer = pl3xPlayer;
        }

        @Override
        public void run() {
            Collection<Claim> claims = pl3xPlayer.getClaims();

            String name = pl3xPlayer.getPlayer().getName();
            if (name == null) {
                name = "Unknown";
            }
            int count = claims.size();

            claims.forEach(claim -> plugin.getClaimManager().deleteClaim(claim, true));

            Bukkit.broadcastMessage(Lang.DELETED_INACTIVE_CLAIMS
                    .replace("{owner}", name)
                    .replace("{count}", Integer.toString(count)));
        }
    }
}
